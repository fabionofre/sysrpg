package sysRPG.domains;

import java.util.Scanner;

/**
 * Classe de formul�rios de instancia��o
 * geral de objetos
 * @author F�bio Onofre
 * @since 25/09/2015
 */
public class Form {
	
	private static Scanner entrada = new Scanner(System.in);
	
	public static Atributos novoAtributos(){
		
		Atributos a = new Atributos();
		
		System.out.println("Digite a for�a\n");
		a.setForca(entrada.nextInt());
		System.out.println("Digite a destreza\n");
		a.setDestreza(entrada.nextInt());
		System.out.println("Digite a constitui��o\n");
		a.setConstituicao(entrada.nextInt());
		System.out.println("Digite a inteligencia\n");
		a.setInteligencia(entrada.nextInt());
		System.out.println("Digite a sabedoria\n");
		a.setSabedoria(entrada.nextInt());
		System.out.println("Digite o carisma\n");
		a.setCarisma(entrada.nextInt());
		
		return a;
		
	}
	
	public static Tendencia novaTendencia(){
		
		Tendencia t = new Tendencia();
		
		/*System.out.println("Digite o nome da tend�ncia em portugu�s\n");
		t.setNomePT(entrada.nextLine());
		System.out.println("Digite o nome da tend�ncia em ingl�s\n");
		t.setNomeING(entrada.nextLine());
		System.out.println("Digite a descri��o da tend�ncia\n");
		t.setDescricao(entrada.nextLine());*/
		System.out.println("Informe o �ndice de moral\n");
		t.setMoral(entrada.nextInt());
		System.out.println("Informe o �ndice de �tica\n");
		t.setEtica(entrada.nextInt());
		
		return t;
		
		
	}
	
	public static Raca novaRaca(){
		
		Raca r = new Raca();
		
		System.out.println("Digite o nome da ra�a\n");
		r.setNome(entrada.next());
		System.out.println("Informe a descri��o da ra�a\n");
		r.setDescricao(entrada.next());
		r.setAlteracaoAtributos(Form.novoAtributos());
		
		return r;
		
	}
	
	public static Pericia novaPericia(){
		
		Pericia p = new Pericia();
		
		System.out.println("Digite o nome da pericia\n");
		p.setNome(entrada.next());
		System.out.println("Informe a descri�a� da pericia\n");
		p.setDescricao(entrada.next());
		
		return p;
		
	}
	
	public static Personagem novoPersonagem(){
		
		Personagem p = new Personagem();
		
		System.out.println("Digite o nome\n");
		p.setNome(entrada.next());
		System.out.println("Digite a idade\n");
		p.setIdade(entrada.nextInt());
		System.out.println("Digite o sexo\n");
		p.setSexo(Sexo.valueOf(entrada.next()));
		
		return p;
		
		
		
	}

}
