/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysRPG.domains;

/**
 * Classe representativa de uma Tendência
 * @author Luke Frozz
 * @since 23/09/2015
 */
public class Tendencia {
	/**
	 * Id da Tend�ncia
	 */
    private Long id;
    /**
     * Nome em portugu�s da Tend�ncia
     */
    private String nomePT;
    /**
     * Nome em ingl�s da Tend�ncia
     */
    private String nomeING;
    /**
     * Descri��o da Tend�ncia
     */
    private String descricao;
    /**
     * �ndice Moral da Tend�ncia
     */
    private Integer moral;
    /**
     * �ndice �tico da Tend�ncia
     */
    private Integer etica;

    /**
     * Getter de Id
     * @return Id da Tend�ncia
     */
    public Long getId() {
        return id;
    }
    /**
     * Setter de Id
     * @param id atribui o Id de Tend�ncia
     */
    public void setId(Long id) {
        this.id = id;
    }
    /**
     * Getter de nomePT
     * @return nome em portugu�s da Tend�ncia
     */
    public String getNomePT() {
        return nomePT;
    }
    /**
     * Setter de nomePT
     * @param nomePT atribui o nome em Portugu�s de Tend�ncia
     */
    public void setNomePT(String nomePT) {
        this.nomePT = nomePT;
    }
    /**
     * Getter de nomeING
     * @return Nome em Ingl�s de Tend�ncia
     */
    public String getNomeING() {
        return nomeING;
    }
    /**
     * Setter de nomeING
     * @param nomeING atribui o nome em Ingl�s de Tend�ncia
     */
    public void setNomeING(String nomeING) {
        this.nomeING = nomeING;
    }
    /**
     * Getter de Descri��o
     * @return Descri��o de Tend�ncia
     */
    public String getDescricao() {
        return descricao;
    }
    /**
     * Setter de Descri��o
     * @param descricao atribui a Descri��o da Tend�ncia
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    /**
     * Getter de Moral
     * @return Eixo Moral da Tend�ncia, essa que pode ser: <br/>
     * <ol>
     * 	  <li>Bom</li>
     * 	  <li>Neutro</li>
     * 	  <li>Mal</li>
     */
    public String getMoral() {
        String typeM = "";
        switch (moral) {
            case 1:
                typeM = "Bom";
                break;
            case 2:
                typeM = "Neutro";
                break;
            case 3:
                typeM = "Mal";
        }
        return typeM;
    }
    /**
     * Setter de Moral
     * @param moral atribui o Eixo Moral da Tend�ncia, essa que pode ser:<br/>
     * <ol>
     * 	  <li>Bom</li>
     * 	  <li>Neutro</li>
     * 	  <li>Mal</li>
     * </ol>
     */
    public void setMoral(Integer moral) {
        this.moral = moral;
    }

    /**
     * Getter de �tica
     * @return Eixo �tico da Tend�ncia, essa que pode ser: <br/>
     * <ol>
     * 	  <li>Leal</li>
     * 	  <li>Neutro</li>
     * 	  <li>Ca�tico</li>
     * </ol>
     */
    public String getEtica() {
        String typeE = "";
        switch (etica) {
            case 1:
                typeE = "Leal";
                break;
            case 2:
                typeE = "Neutro";
                break;
            case 3:
                typeE = "Caótico";
        }
        return typeE;
    }
    /**
     * Setter de �tica
     * @param etica atribui o Eixo �tico da Tend�ncia, esse que pode ser: <br/>
     * <ol>
     * 	  <li>Leal</li>
     * 	  <li>Neutro</li>
     * 	  <li>Ca�tico</li>
     * </ol>
     */
    public void setEtica(Integer etica) {
        this.etica = etica;
    }
    
    /**
     * Construtor padr�o de Tend�ncia
     */
    public Tendencia() {
    	
    }
}
