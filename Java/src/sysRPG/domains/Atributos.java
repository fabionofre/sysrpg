/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysRPG.domains;

/**
 * Classe representativa de atributos
 * @author Luke Frozz
 * @since 21/09/2015
 */
public class Atributos {
	/**
	 * Atributo f�sico de For�a
	 */
    private int forca;
    /**
     * Atributo f�sico de Destreza
     */
    private int destreza;
    /**
     * Atributo F�sico de Constitui��o
     */
    private int constituicao;
    /**
     * Atributo mental de Intelig�ncia
     */
    private int inteligencia;
    /**
     * Atributo mental de Sabedoria
     */
    private int sabedoria;
    /**
     * Atributo mental de Carisma
     */
    private int carisma;

    /**
     * M�todo para identificar qual � o valor
     * do modificador de Habilidade de um Atributo
     * @param attr atributo para a verifica��o
     * @return Modificador de Habilidade do Atributo
     */
    public static Integer mod(Integer attr) {
    	int mod = 0;
    	
    	if (attr <= 1)
    		mod = -5;
    	else {
    		if (attr % 2 == 1)
    			mod = ((attr - 1) / 2) - 5;
    		else
    			mod = (attr / 2) - 5;
    	}
    	
    	return mod;
    }

    /**
     * Getter de For�a
     * @return FOR
     */
    public int getForca() {
        return forca;
    }
    /**
     * Setter de for�a
     * @param forca atribui o valor da For�a
     */
    public void setForca(int forca) {
        this.forca = forca;
    }
    /**
     * Getter de Destreza
     * @return DES
     */
    public int getDestreza() {
        return destreza;
    }
    /**
     * Setter de Destreza
     * @param destreza atribui o valor da Destreza
     */
    public void setDestreza(int destreza) {
        this.destreza = destreza;
    }
    /**
     * Getter de Constitui��o
     * @return CONS
     */
    public int getConstituicao() {
        return constituicao;
    }
    /**
     * Setter de Constitui��o
     * @param constituicao atribui o valor de Constitui��o
     */
    public void setConstituicao(int constituicao) {
        this.constituicao = constituicao;
    }
    /**
     * Getter de Intelig�ncia
     * @return INT
     */
    public int getInteligencia() {
        return inteligencia;
    }
    /**
     * Setter de Intelig�ncia
     * @param inteligencia atribui o valor da Intelig�ncia
     */
    public void setInteligencia(int inteligencia) {
        this.inteligencia = inteligencia;
    }
    /**
     * Getter de Sabedoria
     * @return SAB
     */
    public int getSabedoria() {
        return sabedoria;
    }
    /**
     * Setter de Sabedoria
     * @param sabedoria atribui o valor da Sabedoria
     */
    public void setSabedoria(int sabedoria) {
        this.sabedoria = sabedoria;
    }
    /**
     * Getter de Carisma
     * @return CAR
     */
    public int getCarisma() {
        return carisma;
    }
    /**
     * Setter de Carisma
     * @param carisma Atribui o valor do Carisma
     */
    public void setCarisma(int carisma) {
        this.carisma = carisma;
    }
    
    /**
     * Construtor padr�o de Atributos
     */
    public Atributos() {
    	this.forca = 0;
    	this.destreza = 0;
    	this.constituicao = 0;
    	this.inteligencia = 0;
    	this.sabedoria = 0;
    	this.carisma = 0;
    }
    

}
